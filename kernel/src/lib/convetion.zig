const main = @import("../main.zig");

pub fn phys_to_virt(phys: u64) u64 {
    return phys + main.hhdm_offset;
}

pub fn virt_to_phys(virt: u64) u64 {
    return virt - main.hhdm_offset;
}
