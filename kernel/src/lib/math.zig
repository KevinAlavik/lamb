pub fn divRoundUp(x: usize, y: usize) usize {
    return (x + (y - 1)) / y;
}

pub fn alignUp(x: usize, y: usize) usize {
    return divRoundUp(x, y) * y;
}
