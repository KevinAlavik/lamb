const std = @import("std");
const serial = @import("../../arch/serial.zig");

pub const stdout = std.io.Writer(void, error{}, stdout_print){
    .context = {},
};

pub fn stdout_print(_: void, bytes: []const u8) !usize {
    for (bytes) |byte|
        serial.outb(0x3F8, byte);

    return bytes.len;
}

pub fn print(comptime format: []const u8, args: anytype) void {
    stdout.print(format, args) catch {
        // TODO: Error handling
    };
}

pub fn printNoArgs(comptime str: []const u8) void {
    stdout.print(str, .{}) catch {
        // TODO: Error handling
    };
}
