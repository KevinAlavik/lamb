const std = @import("std");
const io = @import("io.zig");
const cpu = @import("../../arch/cpu.zig");

pub const LambStandardErrors = error{
    OutOfMemory,
    TestFailed,
    NotImpelemented,
    NullValue,
    UnknownFailure,
    InvalidArguments,
};

pub const LambBootErrors = error{
    InvalidRequestResponse,
};

pub const LambErrorCategories = error{
    VirtualMemoryManager,
    PhysicalMemoryManager,
    KernelError,
};

pub fn throw(err: anyerror, c: LambErrorCategories, fatal: bool) void {
    io.print("!>> ERROR: {s}:{s} - FATAL: {s}\n", .{
        @errorName(c),
        @errorName(err),
        if (fatal) "true" else "false",
    });

    if (fatal) {
        cpu.hcf();
    }
}
