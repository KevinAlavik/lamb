const std = @import("std");
const limine = @import("limine");

// System imports
const cpu = @import("arch/cpu.zig");
const idt = @import("arch/idt.zig");

// Memory imports
const pmm = @import("mm/pmm.zig");
const vmm = @import("mm/vmm.zig");
const heap = @import("mm/heap.zig");

// Lib imports
const io = @import("lib/std/io.zig");
const stdout = io.stdout;

// Stuff
const err = @import("lib/std/error.zig");

// Limine stuff
pub export var base_revision: limine.BaseRevision = .{ .revision = 1 };
pub export var memmap_request: limine.MemoryMapRequest = .{};
pub export var hhdmoffset_request: limine.HhdmRequest = .{};

pub var hhdm_offset: u64 = 0;

pub fn panic(message: []const u8, _: ?*std.builtin.StackTrace, _: ?usize) noreturn {
    io.print("Error: \"{s}\"\n", .{message});
    io.printNoArgs("Stack Trace:\n");
    var it = std.debug.StackIterator.init(@returnAddress(), null);
    while (it.next()) |addr| {
        io.print(" - 0x{x:0<16}\n", .{addr});
    }
    cpu.hcf();
}

export fn _start() callconv(.C) noreturn {
    if (!base_revision.is_supported()) {
        cpu.hcf();
    }

    io.printNoArgs("--- Lamb Kernel ---\n");
    if (hhdmoffset_request.response) |hhdmoffset_response| {
        hhdm_offset = hhdmoffset_response.*.offset;
    } else {
        err.throw(err.LambBootErrors.InvalidRequestResponse, err.LambErrorCategories.KernelError, true);
    }
    io.printNoArgs(">>> Initializing IDT...\n");
    idt.init();
    io.printNoArgs(">>> Initialized IDT\n");
    io.printNoArgs(">>> Initializing PMM...\n");
    pmm.init() catch |e| err.throw(e, err.LambErrorCategories.PhysicalMemoryManager, true);

    const a = pmm.request_pages(1);
    defer pmm.free_pages(a, 1);
    if (a == null) err.throw(err.LambStandardErrors.TestFailed, err.LambErrorCategories.KernelError, true) else io.printNoArgs(" >> Test was successful\n");

    io.printNoArgs(">>> Initialized PMM\n");

    io.printNoArgs(">>> Initializing VMM...\n");
    vmm.init() catch |e| err.throw(e, err.LambErrorCategories.VirtualMemoryManager, true);
    io.printNoArgs(">>> Initialized VMM\n\n");

    cpu.hlt();
}
