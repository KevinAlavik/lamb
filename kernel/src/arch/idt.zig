const io = @import("../lib/std/io.zig");
const cpu = @import("cpu.zig");

const Frame = packed struct {
    err: u64,
    rip: u64,
    cs: u64,
    rflags: u64,
    rsp: u64,
    ss: u64,

    regs: packed struct {
        r15: u64,
        r14: u64,
        r13: u64,
        r12: u64,
        r11: u64,
        r10: u64,
        r9: u64,
        r8: u64,
        rdi: u64,
        rsi: u64,
        rbp: u64,
        rbx: u64,
        rdx: u64,
        rcx: u64,
        rax: u64,
    },
};

const Entry = packed struct {
    offset_low: u16,
    selector: u16,
    ist: u8,
    flags: u8,
    offset_middle: u16,
    offset_high: u32,
    zero: u32,

    fn init(base: u64, sel: u16, flags: u8) @This() {
        return .{
            .offset_low = @intCast(base & 0xFFFF),
            .offset_middle = @intCast((base >> 16) & 0xFFFF),
            .offset_high = @intCast((base >> 32) & 0xFFFFFFFF),
            .selector = sel,
            .ist = 0,
            .flags = flags,
            .zero = 0,
        };
    }
};

const Idtr = packed struct {
    limit: u16,
    base: u64,
};

var idt: [256]Entry = undefined;
var idt_p: Idtr = undefined;

pub fn init() void {
    idt_p.limit = @intCast(@sizeOf(Entry) * 255 - 1);
    idt_p.base = @intFromPtr(&idt);

    io.print(" >> Limit: 0x{x:0<4}\n", .{idt_p.limit});
    io.print(" >> Base: 0x{x:0<16}\n", .{idt_p.base});

    asm volatile ("sti");

    inline for (0..32) |i| {
        idt[i] = Entry.init(@intFromPtr(&handler(panic, @intCast(i))), 0x28, 0x8E);
    }

    asm volatile ("lidt (%[idtr])"
        :
        : [idtr] "r" (&idt_p),
    );

    asm volatile ("cli");
}

pub fn handler(comptime trap: fn (*Frame, u8) callconv(.C) void, comptime vector: u8) fn () callconv(.Naked) void {
    return struct {
        fn f() callconv(.Naked) void {
            asm volatile (
                \\ sub $8, %rsp
                \\ mov %rsp, %rdi
                \\ mov %[vector], %sil
                :
                : [vector] "i" (vector),
            );

            // Push all general-purpose registers onto the stack
            asm volatile (
                \\ push %rax
                \\ push %rbx
                \\ push %rcx
                \\ push %rdx
                \\ push %rbp
                \\ push %rsi
                \\ push %rdi
                \\ push %r8
                \\ push %r9
                \\ push %r10
                \\ push %r11
                \\ push %r12
                \\ push %r13
                \\ push %r14
                \\ push %r15
            );

            // Call the trap function
            asm volatile (
                \\ movabs %[handler], %rax
                \\ call *%rax
                :
                : [handler] "p" (trap),
            );

            // Pop all general-purpose registers from the stack
            asm volatile (
                \\ pop %r15
                \\ pop %r14
                \\ pop %r13
                \\ pop %r12
                \\ pop %r11
                \\ pop %r10
                \\ pop %r9
                \\ pop %r8
                \\ pop %rdi
                \\ pop %rsi
                \\ pop %rbp
                \\ pop %rdx
                \\ pop %rcx
                \\ pop %rbx
                \\ pop %rax
            );

            // Return from the interrupt handler
            asm volatile (
                \\ add $8, %rsp
                \\ iretq
            );
        }
    }.f;
}

comptime {
    @export(panic, .{ .name = "panic" });
}
fn panic(frame: *Frame, vector: u8) callconv(.C) void {
    io.print(
        "Panic! Vector: {} ({s}) @ 0x{x:0<16}\n",
        .{ vector, cpu.exeptions[vector], frame.rip },
    );

    cpu.hcf();
}
