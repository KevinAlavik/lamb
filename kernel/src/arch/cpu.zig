pub fn hcf() noreturn {
    asm volatile ("cli");
    hlt();
}

pub fn hlt() noreturn {
    while (true) {
        asm volatile ("hlt");
    }
}

pub fn int(vec: u8) void {
    asm volatile ("int %[i]"
        :
        : [i] "i" (vec),
    );
}

pub var exeptions: [32][]const u8 = .{ "Division By Zero", "Debug", "Nonmaskable Interrupt", "Breakpoint", "Overflow", "Bound Range Exceeded", "Invalid Opcode", "Device Not Available", "Double Fault", "Coprocessor Segment Overrun", "Invalid TSS", "Segment Not Present", "Stack Segment Fault", "General Protection Fault", "Page Fault", "Reserved", "x87 FPU Error", "Alignment Check", "Machine Check", "Simd Exception", "Virtualization Exception", "Control Protection Exception", "Reserved", "Reserved", "Reserved", "Reserved", "Reserved", "Reserved", "Hypervisor Injection Exception", "VMM Communication Exception", "Security Exception", "Reserved" };
