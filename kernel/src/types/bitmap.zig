pub fn set(bitmap: []u8, bit: u64) void {
    bitmap[bit / 8] |= @as(u8, 1) << @intCast(bit % 8);
}

pub fn clear(bitmap: []u8, bit: u64) void {
    bitmap[bit / 8] &= ~(@as(u8, 1) << @intCast(bit % 8));
}

pub fn get(bitmap: []u8, bit: u64) u8 {
    return bitmap[bit / 8] & (@as(u8, 1) << @intCast(bit % 8));
}
