const limine = @import("limine");
const std = @import("std");

const io = @import("../lib/std/io.zig");
const main = @import("../main.zig");
const err = @import("../lib/std/error.zig");

const bit = @import("../types/bitmap.zig");

const math = @import("../lib/math.zig");

pub const PAGE_SIZE: u64 = 4096;

var bitmap: []u8 = &[_]u8{};
var bitmap_pages: u64 = 0;
var bitmap_size: u64 = 0;

pub fn init() !void {
    var entries: []*limine.MemoryMapEntry = undefined;

    if (main.memmap_request.response) |memmap_response| {
        entries = memmap_response.entries();
    } else {
        return err.LambBootErrors.InvalidRequestResponse;
    }

    var top_address: u64 = 0;
    var higher_address: u64 = 0;

    for (entries) |entry| {
        if (entry.kind == limine.MemoryMapEntryType.usable) {
            top_address = entry.base + entry.length;
            if (top_address > higher_address) higher_address = top_address;
            io.print(" >> Usable Entry. Entry: 0x{x:0>16}, Top Address: 0x{x:0>16}, Higher Address: 0x{x:0>16}\n", .{ entry.base, top_address, higher_address });
        }
    }

    bitmap_pages = higher_address / PAGE_SIZE;
    bitmap_size = math.alignUp(bitmap_pages / 8, PAGE_SIZE);

    for (entries) |entry| {
        if (entry.kind == limine.MemoryMapEntryType.usable) {
            if (entry.length >= bitmap_size) {
                bitmap = @as([*]u8, @ptrFromInt(entry.base + main.hhdm_offset))[0..bitmap_size];
                @memset(bitmap, 0xFF);
                entry.base += bitmap_size;
                entry.length -= bitmap_size;
                break;
            }
        }
    }

    for (entries) |entry| {
        if (entry.kind == limine.MemoryMapEntryType.usable) {
            var i: u64 = 0;
            while (i != entry.length) : (i += PAGE_SIZE) {
                bit.clear(bitmap, (entry.base + i) / PAGE_SIZE);
            }
        }
    }

    return;
}

pub fn request_pages(num: u64) ?*anyopaque {
    var last_allocated_index: u64 = 0;

    while (true) {
        if (bit.get(bitmap, last_allocated_index) == 0) {
            var consecutive_free_pages: u64 = 1;

            for (1..num) |i| {
                if (bit.get(bitmap, last_allocated_index + i) == 0) {
                    consecutive_free_pages += 1;
                } else {
                    consecutive_free_pages = 0;
                    break;
                }
            }

            if (consecutive_free_pages == num) {
                for (0..num) |i| {
                    bit.set(bitmap, last_allocated_index + i);
                }

                return @ptrFromInt(last_allocated_index * PAGE_SIZE);
            }
        }

        last_allocated_index += 1;

        if (last_allocated_index >= bitmap_pages) {
            return null;
        }
    }
}

pub fn free_pages(ptr: ?*anyopaque, num: u64) void {
    const start_bit_idx = (@as(u64, @intFromPtr(ptr)) / PAGE_SIZE);

    for (0..num) |i| {
        bit.clear(bitmap, start_bit_idx + i);
    }
}
