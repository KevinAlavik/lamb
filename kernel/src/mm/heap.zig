const std = @import("std");

const err = @import("../lib/std/error.zig");

const pmm = @import("pmm.zig");

pub fn malloc(size: usize) !?*anyopaque {
    _ = size;
    return err.LambStandardErrors.NotImpelemented;
}
