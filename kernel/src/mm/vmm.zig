const std = @import("std");
const pmm = @import("pmm.zig");
const err = @import("../lib/std/error.zig");
const io = @import("../lib/std/io.zig");
const conv = @import("../lib/convetion.zig");

pub const PML4E = packed struct {
    Present: u1,
    ReadWrite: u1,
    UserSuper: u1,
    WriteThrough: u1,
    CacheDisable: u1,
    Accessed: u1,
    Available0: u1,
    Reserved0: u1,
    Available1: u4,
    Address: u40,
    Available2: u11,
    ExecuteDisable: u1,
};

pub const PML3E = packed struct {
    Present: u1,
    ReadWrite: u1,
    UserSuper: u1,
    WriteThrough: u1,
    CacheDisable: u1,
    Accessed: u1,
    Available0: u1,
    PageSize: u1,
    Available1: u4,
    Address: u40,
    Available2: u11,
    ExecuteDisable: u1,
};

pub const PML2E = packed struct {
    Present: u1,
    ReadWrite: u1,
    UserSuper: u1,
    WriteThrough: u1,
    CacheDisable: u1,
    Accessed: u1,
    Available0: u1,
    PageSize: u1,
    Available1: u4,
    Address: u40,
    Available2: u11,
    ExecuteDisable: u1,
};

pub const PML1E = packed struct {
    Present: u1,
    ReadWrite: u1,
    UserSuper: u1,
    WriteThrough: u1,
    CacheDisable: u1,
    Accessed: u1,
    Dirty: u1,
    PAT: u1,
    Global: u1,
    Available0: u3,
    Address: u40,
    Available1: u7,
    ProtKey: u4,
    ExecuteDisable: u1,
};

pub const Flags = enum(u32) {
    Present = 1,
    Write = 2,
    User = 4,
    ExecuteDisable = 0x8000000,
};

const Level4Group = struct {
    entries: [512]PML4E,
};

var PML4Array: Level4Group = undefined;
pub fn map(vaddr: u64, paddr: u64, flags: u32) !void {
    if (flags == 0) {
        return err.LambStandardErrors.InvalidArguments;
    }
    const pt: u16 = @intCast((vaddr & 0x0000001FF000) >> 12);
    const pd: u16 = @intCast((vaddr & 0x00003FE00000) >> 21);
    const pdp: u16 = @intCast((vaddr & 0x007FC0000000) >> 30);
    const pml4: u16 = @intCast((vaddr & 0xFF8000000000) >> 39);

    var PML4: PML4E = PML4Array.entries[pml4];
    if (PML4.Present == 0) {
        const temp = @as(u64, @intCast(flags & 0x0FFF)) | (@as(u64, @intCast(flags & 0x07FF0000)) << 36);
        PML4 = @bitCast(temp);
        PML4.Present = 1;
        PML4.Address = @truncate(@intFromPtr(pmm.request_pages(1) orelse return error.OutOfMemory) >> 12);
        @memset(@as([*]u8, @ptrFromInt(conv.phys_to_virt(PML4.Address << 12)))[0..4096], 0);
    } else {
        var temp: u64 = @intFromPtr(&PML4);
        temp |= flags & 0xFFF;
        temp |= flags & 0x7FF0000;
        PML4Array.entries[pml4] = @bitCast(temp);
    }

    var PML3: PML3E = @as([*]PML3E, @ptrFromInt(conv.phys_to_virt(PML4.Address << 12)))[pdp];
    if (PML3.Present == 0) {
        const temp = @as(u64, @intCast(flags & 0x0FFF)) | (@as(u64, @intCast(flags & 0x07FF0000)) << 36);
        PML3 = @bitCast(temp);
        PML3.Present = 1;
        PML3.Address = @truncate(@intFromPtr(pmm.request_pages(1) orelse return error.OutOfMemory) >> 12);
        @memset(@as([*]u8, @ptrFromInt(conv.phys_to_virt(PML3.Address << 12)))[0..4096], 0);
        (@as([*]PML3E, @ptrFromInt(conv.phys_to_virt(PML4.Address << 12)))[pdp]) = PML3;
    } else {
        var temp: u64 = @intFromPtr(&PML3);
        temp |= flags & 0xFFF;
        temp |= @as(u64, (flags & 0x7FF0000)) << 36;
        (@as([*]PML3E, @ptrFromInt(conv.phys_to_virt(PML4.Address << 12)))[pdp]) = @bitCast(temp);
    }

    var PML2: PML2E = @as([*]PML2E, @ptrFromInt(conv.phys_to_virt(PML3.Address << 12)))[pd];
    if (PML2.Present == 0) {
        const temp = @as(u64, @intCast(flags & 0x0FFF)) | (@as(u64, @intCast(flags & 0x07FF0000)) << 36);
        PML2 = @bitCast(temp);
        PML2.Present = 1;
        PML2.Address = @truncate(@intFromPtr(pmm.request_pages(1) orelse return error.OutOfMemory) >> 12);
        @memset(@as([*]u8, @ptrFromInt(conv.phys_to_virt(PML2.Address << 12)))[0..4096], 0);
        (@as([*]PML2E, @ptrFromInt(conv.phys_to_virt(PML3.Address << 12)))[pd]) = PML2;
    } else {
        var temp: u64 = @intFromPtr(&PML2);
        temp |= flags & 0xFFF;
        temp |= @as(u64, (flags & 0x7FF0000)) << 36;
        (@as([*]PML2E, @ptrFromInt(conv.phys_to_virt(PML3.Address << 12)))[pd]) = @bitCast(temp);
    }

    const temp = @as(u64, @intCast(flags & 0x0FFF)) | (@as(u64, @intCast(flags & 0x07FF0000)) << 36);
    var PML1: PML1E = @bitCast(temp);
    PML1.Address = @intCast(paddr >> 12);

    (@as([*]PML1E, @ptrFromInt(conv.phys_to_virt(PML2.Address << 12)))[pt]) = PML1;
}

fn unmap(vaddr: u64) bool {
    const pt: u16 = @intCast((vaddr & 0x0000001FF000) >> 12);
    const pd: u16 = @intCast((vaddr & 0x00003FE00000) >> 21);
    const pdp: u16 = @intCast((vaddr & 0x007FC0000000) >> 30);
    const pml4: u16 = @intCast((vaddr & 0xFF8000000000) >> 39);

    const PML4: PML4E = PML4Array.entries[pml4];
    if (!(PML4.Present)) {
        return false;
    }
    const PML3: PML3E = @as(PML3E, conv.phys_to_virt(PML4.Address << 12)[pdp]);
    if (!(PML3.Present)) {
        return false;
    }
    const PML2: PML2E = @as(PML2E, conv.phys_to_virt(PML3.Address << 12)[pd]);
    if (!(PML2.Present)) {
        return false;
    }
    const PML1: PML1E = @as(PML1E, conv.phys_to_virt(PML2.Address << 12)[pt]);
    PML1.Address = 0;
    return true;
}

fn map_range(virt: *void, phys: *void, virt_end: *void, perms: u32) void {
    var v = virt;
    while (v < virt_end) : (v += 4096) {
        map(@intFromPtr(v), @intFromPtr(phys), perms);
        phys = @ptrFromInt(@intFromPtr(phys) + 4096);
    }
}

pub fn init() !void {
    for (0..512) |i| {
        PML4Array.entries[i] = .{
            .Present = 0,
            .ReadWrite = 0,
            .UserSuper = 0,
            .WriteThrough = 0,
            .CacheDisable = 0,
            .Accessed = 0,
            .Available0 = 0,
            .Reserved0 = 0,
            .Available1 = 0,
            .Address = 0,
            .Available2 = 0,
            .ExecuteDisable = 0,
        };
    }

    var addr: u64 = 0;
    while (addr < (4 * 1024 * 1024 * 1024)) : (addr += 0x1000) {
        map(conv.phys_to_virt(addr), addr, @intFromEnum(Flags.Present) | @intFromEnum(Flags.Write) | @intFromEnum(Flags.ExecuteDisable)) catch |e| {
            return e;
        };
    }
}
