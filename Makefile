override MAKEFLAGS += -rR

override IMAGE_NAME := Lamb

define DEFAULT_VAR =
    ifeq ($(origin $1),default)
        override $(1) := $(2)
    endif
    ifeq ($(origin $1),undefined)
        override $(1) := $(2)
    endif
endef

override DEFAULT_KZIGFLAGS := -Doptimize=ReleaseFast
$(eval $(call DEFAULT_VAR,KZIGFLAGS,$(DEFAULT_KZIGFLAGS)))

.PHONY: all
all: $(IMAGE_NAME).iso

.PHONY: all-hdd
all-hdd: $(IMAGE_NAME).hdd

.PHONY: run
run: $(IMAGE_NAME).iso
	@qemu-system-x86_64 -M q35 -m 2G  --serial stdio -cdrom $(IMAGE_NAME).iso -boot d


.PHONY: run-no-display
run-no-display: $(IMAGE_NAME).iso
	@qemu-system-x86_64 -M q35 -m 2G --display none --serial stdio -cdrom $(IMAGE_NAME).iso -boot d

.PHONY: run-uefi
run-uefi: ovmf $(IMAGE_NAME).iso
	@qemu-system-x86_64 -M q35 -m 2G -bios ovmf/OVMF.fd --serial stdio  -cdrom $(IMAGE_NAME).iso -boot d

.PHONY: run-hdd
run-hdd: $(IMAGE_NAME).hdd
	@qemu-system-x86_64 -M q35 -m 2G --serial stdio  -hda $(IMAGE_NAME).hdd

.PHONY: run-hdd-uefi
run-hdd-uefi: ovmf $(IMAGE_NAME).hdd
	@qemu-system-x86_64 -M q35 -m 2G -bios ovmf/OVMF.fd --serial stdio -hda $(IMAGE_NAME).hdd 

ovmf:
	@mkdir -p ovmf
	@cd ovmf && curl -Lo OVMF.fd https://retrage.github.io/edk2-nightly/bin/RELEASEX64_OVMF.fd

limine:
	@git clone https://github.com/limine-bootloader/limine.git --branch=v7.x-binary --depth=1
	@$(MAKE) -C limine

.PHONY: kernel
kernel:
	@cd kernel && zig build -Doptimize=ReleaseFast $(KZIGFLAGS)

$(IMAGE_NAME).iso: limine kernel
	@rm -rf iso_root
	@mkdir -p iso_root/boot
	@cp kernel/zig-out/bin/lambx64.elf iso_root/boot/lambx64.elf 2>&1
	@mkdir -p iso_root/boot/limine
	@cp conf/limine.cfg limine/limine-bios.sys limine/limine-bios-cd.bin limine/limine-uefi-cd.bin iso_root/boot/limine/ 2>&1
	@mkdir -p iso_root/EFI/BOOT
	@cp limine/BOOTX64.EFI iso_root/EFI/BOOT/ 2>&1
	@cp limine/BOOTIA32.EFI iso_root/EFI/BOOT/ 2>&1
	@xorriso -as mkisofs -b boot/limine/limine-bios-cd.bin \
		-no-emul-boot -boot-load-size 4 -boot-info-table \
		--efi-boot boot/limine/limine-uefi-cd.bin \
		-efi-boot-part --efi-boot-image --protective-msdos-label \
		iso_root -o $(IMAGE_NAME).iso > /dev/null 2>&1
	@./limine/limine bios-install $(IMAGE_NAME).iso > /dev/null 2>&1
	@rm -rf iso_root

$(IMAGE_NAME).hdd: limine kernel
	@rm -f $(IMAGE_NAME).hdd
	@dd if=/dev/zero bs=1M count=0 seek=64 of=$(IMAGE_NAME).hdd
	@sgdisk $(IMAGE_NAME).hdd -n 1:2048 -t 1:ef00
	@./limine/limine bios-install $(IMAGE_NAME).hdd
	@mformat -i $(IMAGE_NAME).hdd@@1M
	@mmd -i $(IMAGE_NAME).hdd@@1M ::/EFI ::/EFI/BOOT ::/boot ::/boot/limine
	@mcopy -i $(IMAGE_NAME).hdd@@1M kernel/zig-out/bin/lambx64.elf ::/boot
	@mcopy -i $(IMAGE_NAME).hdd@@1M conf/limine.cfg limine/limine-bios.sys ::/boot/limine
	@mcopy -i $(IMAGE_NAME).hdd@@1M limine/BOOTX64.EFI ::/EFI/BOOT
	@mcopy -i $(IMAGE_NAME).hdd@@1M limine/BOOTIA32.EFI ::/EFI/BOOT

.PHONY: clean
clean:
	@rm -rf iso_root $(IMAGE_NAME).iso $(IMAGE_NAME).hdd
	@rm -rf kernel/zig-cache kernel/zig-out

.PHONY: distclean
distclean: clean
	@rm -rf limine ovmf